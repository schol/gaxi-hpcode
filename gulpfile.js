const gulp = require("gulp");

/* sass */
const sass = require("gulp-sass")(require("node-sass"));
const plumber = require("gulp-plumber");
const notify = require("gulp-notify");
const sassGlob = require("gulp-sass-glob");
const mmq = require("gulp-merge-media-queries");
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
const cssdeclsort = require("css-declaration-sorter");

/* browser-sync */
const ejs = require("gulp-ejs");
const rename = require("gulp-rename");
const browserSync = require("browser-sync");

/* ejs */
const htmlbeautify = require("gulp-html-beautify");

/* javascript */
const uglify = require("gulp-uglify");
const babel = require("gulp-babel");

gulp.task("sass", function() {
	return gulp
		.src("./src/**/*.scss")
		.pipe(plumber({ errorHandler: notify.onError("Error: <%= error.message %>") }))
		.pipe(sassGlob())
		.pipe(sass({ outputStyle: "expanded" }))
		.pipe(postcss([autoprefixer({ grid: true })]))
		.pipe(postcss([cssdeclsort({ order: "alphabetical" })]))
		.pipe(mmq())
		.pipe(gulp.dest("./public"));
});

gulp.task("js", function(done) {
	gulp
		.src("./src/**/*.js")
		.pipe(
			babel({
				presets: ["@babel/env"]
			})
		)
		.pipe(gulp.dest("./public"));
	done();
});

gulp.task("ejs", function(done) {
	const options = {
		indent_size: 2,
		indent_with_tabs: true
	};

	gulp
		.src(["src/**/*.ejs", "!src/**/_*.ejs"])
		.pipe(ejs({}))
		.pipe(rename({ extname: ".html" }))
		.pipe(htmlbeautify(options))
		.pipe(gulp.dest("./public"));
	done();
});

gulp.task("watch", function(done) {
	gulp.watch("./src/**/*.scss", gulp.task("sass"));
	gulp.watch("./src/**/*.scss", gulp.task("bs-reload"));
	gulp.watch("./src/**/*.js", gulp.task("bs-reload"));
	gulp.watch("./src/**/*.js", gulp.task("js"));
	gulp.watch("./src/**/*.ejs", gulp.task("ejs"));
	gulp.watch("./src/**/*.ejs", gulp.task("bs-reload"));
	gulp.watch("./public/**/*.html", gulp.task("bs-reload"));
});

gulp.task("browser-sync", function(done) {
	browserSync.init({
		server: {
			baseDir: "./public",
			index: "index.html"
		}
	});
	done();
});

gulp.task("bs-reload", function(done) {
	browserSync.reload();
	done();
});

gulp.task("default", gulp.series(gulp.parallel("browser-sync", "watch")));
