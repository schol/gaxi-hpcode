const drawerOpenButton = document.querySelector('#js-drawer-open');
const drawerContent = document.querySelector('#js-drawer-content');
const drawerBackground = document.querySelector('#js-drawer-bg');

drawerOpenButton.addEventListener('click', function(event) {
	event.preventDefault();
	drawerOpenButton.classList.toggle('is-active');
	drawerContent.classList.toggle('is-active');
	drawerBackground.classList.toggle('is-active');
});

drawerBackground.addEventListener('click', function(event) {
	event.preventDefault();
	drawerOpenButton.classList.toggle('is-active');
	drawerContent.classList.toggle('is-active');
	drawerBackground.classList.toggle('is-active');
});

const rankingTabPrev = document.querySelector('#js-tab-prev');
const rankingTabNext = document.querySelector('#js-tab-next');
const rankingTab = document.querySelector('#js-ranking-tab');
const rankingTabWidth = rankingTab.clientWidth;

rankingTabPrev.addEventListener('click', function(event) {
	event.preventDefault();
	rankingTab.scrollLeft = 0;
});

rankingTabNext.addEventListener('click', function(event) {
	event.preventDefault();
	rankingTab.scrollLeft = rankingTabWidth;
});
