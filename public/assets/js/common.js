"use strict";

var drawerOpenButton = document.querySelector('#js-drawer-open');
var drawerContent = document.querySelector('#js-drawer-content');
var drawerBackground = document.querySelector('#js-drawer-bg');
drawerOpenButton.addEventListener('click', function (event) {
  event.preventDefault();
  drawerOpenButton.classList.toggle('is-active');
  drawerContent.classList.toggle('is-active');
  drawerBackground.classList.toggle('is-active');
});
drawerBackground.addEventListener('click', function (event) {
  event.preventDefault();
  drawerOpenButton.classList.toggle('is-active');
  drawerContent.classList.toggle('is-active');
  drawerBackground.classList.toggle('is-active');
});
var rankingTabPrev = document.querySelector('#js-tab-prev');
var rankingTabNext = document.querySelector('#js-tab-next');
var rankingTab = document.querySelector('#js-ranking-tab');
var rankingTabWidth = rankingTab.clientWidth;
rankingTabPrev.addEventListener('click', function (event) {
  event.preventDefault();
  rankingTab.scrollLeft = 0;
});
rankingTabNext.addEventListener('click', function (event) {
  event.preventDefault();
  rankingTab.scrollLeft = rankingTabWidth;
});